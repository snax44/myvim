# myvim

Only my vim stuff.  

```bash
mkdir ~/projects && cd ~/projects
git clone https://gitlab.com/snax44/myvim.git
ln -s ~/projects/myvim/.vim/ ~/.vim
```

# Sources

- [nftables](https://github.com/nfnty/vim-nftables)  
- [OpenWRT / UCI](https://github.com/cmcaine/vim-uci)  
- [Jinja](https://github.com/Glench/Vim-Jinja2-Syntax)  
- [Proxy PAC Files](https://github.com/vim-scripts/pac.vim)  
